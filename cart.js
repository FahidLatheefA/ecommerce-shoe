/*Function to find the total price*/
function calculateAmount(val) {
  var tot_price = val * 32.13;
  /*display the result*/
  var divobj = document.getElementById('tot_amount');
  /* Round to 2 decimals*/
  divobj.value = tot_price.toFixed(2);
  divobj.value = '$' + divobj.value.toString();
  /* Storing the item */
  localStorage.setItem("total_price", divobj.value);

}